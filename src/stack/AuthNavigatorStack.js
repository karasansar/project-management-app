import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { Login } from "../screens/Login";
import { Register } from "../screens/Register";

const Auth = createStackNavigator();

export const AuthNavigatorStack = () => {
  return (
    <Auth.Navigator
      initialRouteName={"Login"}
      screenOptions={{ headerShown: false }}
    >
      <Auth.Screen name={"Login"} component={Login} />
      <Auth.Screen name={"Register"} component={Register} />
    </Auth.Navigator>
  );
};
