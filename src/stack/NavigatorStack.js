import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { ListProject } from "../screens/ListProject";
import { AddProject } from "../screens/AddProject";
import { UpdateProject } from "../screens/UpdateProject";
import { DetailProjects } from "../screens/DetailProject";
import { MembersInProject } from "../screens/MembersInProject";
import { BugProject } from "../screens/BugProject";
import { AddBugProject } from "../screens/AddBugProject";
import { ToDoProject } from "../screens/ToDoProject";

const Stack = createStackNavigator();
export const NavigatorStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ListProject" component={ListProject} />
      <Stack.Screen name="AddProject" component={AddProject} />
      <Stack.Screen name="UpdateProject" component={UpdateProject} />
      <Stack.Screen name="DetailProject" component={DetailProjects} />
      <Stack.Screen name="MemberProject" component={MembersInProject} />
      <Stack.Screen name="BugProject" component={BugProject} />
      <Stack.Screen name="AddBugProject" component={AddBugProject} />
      <Stack.Screen name="ToDoProject" component={ToDoProject} />
    </Stack.Navigator>
  );
};
