import React from "react";
import { db } from "./db";

export const ProjectDb = async () => {
  await fetch(db)
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.log(error);
    });
};
