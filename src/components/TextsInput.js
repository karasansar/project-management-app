import React from "react";
import { Dimensions } from "react-native";
import { Input } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";

export const TextsInput = ({ label, icon, ...props }) => {
  const width = Dimensions.get("window").width;
  return (
    <Input
      autoCapitalize="none"
      inputContainerStyle={{ width: width - 20 }}
      label={label}
      {...props}
      leftIcon={<Icon name={icon} size={24} color="#f50" />}
    />
  );
};
