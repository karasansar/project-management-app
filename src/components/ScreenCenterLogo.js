import React from "react";
import { Dimensions, Image, Text, View } from "react-native";
import { Asset } from "expo-asset";

export const ScreenCenterLogo = () => {
  const width = Dimensions.get("screen").width;
  const height = Dimensions.get("screen").height;
  return (
    <View
      style={{
        position: "absolute",
        top: height / 2,
        left: width / 2.2,
        justifyContent: "center",
        alignItems: "center",
        zIndex: 20,
      }}
    >
      <Image
        source={Asset.fromModule(require("../../assets/download.jpg"))}
        width={50}
        height={50}
      />
    </View>
  );
};
