import React from "react";
import { Text, View } from "react-native";

export const Error = () => {
  return (
    <View style={{alignItems: 'center'}}>
      <Text style={{ color: "red", fontSize: 18 }}>Hatalı işlem!</Text>
    </View>
  );
};
