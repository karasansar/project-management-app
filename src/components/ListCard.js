import React from "react";
import { CardSeven } from "react-native-card-ui";
import { Alert, View } from "react-native";
export const ListCard = ({ navigation, item }) => {
  return (
    <View style={{ zIndex: 500 }}>
      <CardSeven
        title={item.title.substring(0, 20) + "..."}
        subTitle={item.sub_title.substring(0, 40) + "..."}
        image={item.image}
        icon1={"share"}
        iconColor1={"#fff"}
        iconBackground1={"#f50"}
        onClicked1={() => {
          navigation.navigate("DetailProject", {
            data: item,
          });
        }}
        icon2={"edit"}
        iconColor2={"#fff"}
        iconBackground2={"#517fa4"}
        onClicked2={() => {
          navigation.navigate("UpdateProject");
        }}
        icon3={"trash"}
        iconColor3={"#fff"}
        iconBackground3={"#e60000"}
        onClicked3={() => {
          //buraya eminmisiniz sorulu alert gelecek
          Alert.alert(
            '"' + item.title + '"' + " Silme İşlemi",
            "Bu projeyi silmek istediğinize eminmisiniz?",
            [
              {
                text: "İptal",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              { text: "Evet", onPress: () => console.log("OK Pressed") },
            ],
            { cancelable: false }
          );
        }}
      />
    </View>
  );
};
