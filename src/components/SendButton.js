import React from "react";
import { Button } from "react-native-elements";
import { Dimensions } from "react-native";

export const SendButton = ({...props }) => {
  const width = Dimensions.get("window").width;
  return (
    <Button
      containerStyle={{ width: width - 20 }}
      {...props}
    />
  );
};
