import React from "react";
import { ActivityIndicator, Text, View } from "react-native";

export const Loading = () => {
  return (
    <View
      style={{
        zIndex: 15000,
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <ActivityIndicator
        size="large"
        color="#f50"
        animating={true}
        textContent={"Loading..."}
      />
    </View>
  );
};
