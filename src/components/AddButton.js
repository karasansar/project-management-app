import React from "react";
import { View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export const AddButton = ({ navigation }) => {
  return (
    <View
      style={{
        flex: 1,
        position: "absolute",
        bottom: 50,
        right: 15,
        flexDirection: "row",
        zIndex: 1005,
      }}
    >
      <Icon
        raised
        name="add-circle"
        type="ionicon"
        color="#f50"
        style={{
          fontSize: 70,
        }}
        onPress={() => {
          navigation.navigate("AddProject");
        }}
      />
    </View>
  );
};
