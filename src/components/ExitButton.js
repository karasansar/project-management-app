import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";

export const ExitButton = ({ navigation }) => {
  return (
    <Icon
      raised
      name="times-circle"
      type="FontAwesome"
      color="#f50"
      style={{
        fontSize: 50,
        position: "absolute",
        top: 30,
        right: 25,
        zIndex: 500,
      }}
      onPress={() => {
        navigation.pop();
      }}
    />
  );
};
