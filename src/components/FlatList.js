import React from "react";
import { FlatList, Text } from "react-native";
import { ListCard } from "./ListCard";
console.disableYellowBox = true;
export const FlatLists = ({ navigation, data }) => {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => (
        <ListCard navigation={navigation} item={item} />
      )}
      keyExtractor={(item) => item.id.toString()}
    />
  );
};
