import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { NavigatorStack } from "./stack/NavigatorStack";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AuthContext } from "./auth/AuthContext";
import { AuthNavigatorStack } from "./stack/AuthNavigatorStack";
import expo from "expo";
import { db } from "./db/db";
import axios from "axios";

export default function App({ navigation }) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case "RESTORE_TOKEN":
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case "SIGN_IN":
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case "SIGN_OUT":
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem("userToken");
      } catch (e) {
        console.log(e);
      }

      dispatch({ type: "RESTORE_TOKEN", token: userToken });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data, setLoading, setError) => {
        setLoading(true);
        await axios
          .post(db + "login", {
            email: data.email,
            password: data.password,
          })
          .then(function (response) {
            setTimeout(() => {
              dispatch({ type: "SIGN_IN", token: response.data.token });
              AsyncStorage.setItem("userToken", response.data.token);
              setLoading(false);
            }, 1000);
          })
          .catch(function (error) {
            setTimeout(() => {
              setLoading(false);
              setError(true);
            }, 1000);
          });
      },
      signOut: async () => {
        dispatch({ type: "SIGN_OUT" });
        await AsyncStorage.removeItem("userToken");
      },
      signUp: async (data, setLoading, setError) => {
        setLoading(true);
        await axios
          .post(db + "register", {
            name: data.name,
            email: data.email,
            password: data.password,
            password_confirmation: data.password_confirmation,
          })
          .then(function (response) {
            setTimeout(() => {
              dispatch({ type: "SIGN_IN", token: response.data.token });
              AsyncStorage.setItem("userToken", response.data.token);
              setLoading(false);
            }, 1000);
          })
          .catch(function (error) {
            setTimeout(() => {
              setLoading(false);
              setError(true);
            }, 1000);
          });
      },
    }),
    []
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {state.userToken == null ? <AuthNavigatorStack /> : <NavigatorStack />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
