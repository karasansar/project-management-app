import React from "react";
import { ScrollView, View } from "react-native";
import { Text, Avatar, Image, Icon } from "react-native-elements";
import { ExitButton } from "../components/ExitButton";

export const DetailProjects = ({ navigation, route }) => {
  const item = route.params;
  const obj = item.data;

  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <ExitButton navigation={navigation} />

      <ScrollView showsVerticalScrollIndicator={false} style={{ margin: 15 }}>
        {/* Title */}
        <Text h3 style={{ padding: 10 }}>
          {obj.title}
        </Text>

        {/* Avatar */}
        <View
          style={{
            alignItems: "center",
            flexDirection: "row",
            padding: 10,
          }}
        >
          <Avatar rounded source={obj.creator} size={"small"} />
          <Text style={{ left: 5, fontSize: 16, fontWeight: "bold" }}>
            {obj.creator_name}
          </Text>
          <Text style={{ left: 15, fontSize: 12, opacity: 0.4 }}>
            Oluşturma tarihi - {obj.created_at}
          </Text>
        </View>

        {/* Button Grup */}
        <View
          style={{
            flexDirection: "row",
            padding: 10,
          }}
        >
          <Icon
            size={21}
            reverse
            name="body"
            type="ionicon"
            color="#517fa4"
            onPress={() => navigation.navigate("MemberProject")}
          />
          <Icon
            size={21}
            reverse
            name="bug"
            type="ionicon"
            color="#e60000"
            onPress={() => navigation.navigate("BugProject")}
          />
          <Icon
            size={21}
            reverse
            name="clipboard"
            type="ionicon"
            color="#517fa4"
            onPress={() => navigation.navigate("ToDoProject")}
          />
          <Icon
            size={21}
            reverse
            name="add"
            type="ionicon"
            color="#f50"
            onPress={() => navigation.navigate("AddBugProject")}
          />
        </View>

        {/* Image */}

        <View style={{ padding: 10 }}>
          <Image
            source={obj.image}
            style={{
              width: "100%",
              height: 200,
              borderRadius: 5,
            }}
          />
        </View>

        {/* Description */}

        <View style={{ padding: 10 }}>
          <Text style={{ fontSize: 16 }}>{obj.sub_title}</Text>
        </View>
      </ScrollView>
    </View>
  );
};
