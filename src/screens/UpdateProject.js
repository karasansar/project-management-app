import React from "react";
import { Text, View } from "react-native";
import { ExitButton } from "../components/ExitButton";

export const UpdateProject = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <ExitButton navigation={navigation} />
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Proje Güncelleme Sayfası</Text>
      </View>
    </View>
  );
};
