import React from "react";
import { Asset } from "expo-asset";
import { ScrollView, View, Platform } from "react-native";
import { Header } from "react-native-elements";
import { AddButton } from "../components/AddButton";
import { FlatLists } from "../components/FlatList";
import { AuthContext } from "../auth/AuthContext";

const DATA = [
  {
    id: 1,
    title: "Project Management Projesi",
    sub_title:
      "Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır.",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Doğukan Karasansar",
    image: Asset.fromModule(require("../../assets/test2.png")),
    created_at: "11.04.2021",
  },
  {
    id: 2,
    title: "Second Item",
    sub_title: "bbbbbbbbbbbb",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Doğukan",
    image: Asset.fromModule(require("../../assets/test-image.png")),
    created_at: "11.04.2021",
  },
  {
    id: 3,
    title: "Third Item",
    sub_title: "bbbbbbbbbbbb",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Emre",
    image: Asset.fromModule(require("../../assets/test-image.png")),
    created_at: "11.04.2021",
  },
  {
    id: 4,
    title: "Third Item",
    sub_title: "bbbbbbbbbbbb",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Mikail",
    image: Asset.fromModule(require("../../assets/test-image.png")),
    created_at: "11.04.2021",
  },
  {
    id: 5,
    title: "Third Item",
    sub_title: "bbbbbbbbbbbb",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Enes",
    image: Asset.fromModule(require("../../assets/test-image.png")),
    created_at: "11.04.2021",
  },
  {
    id: 6,
    title: "Project Management Projesi test 121232 dasdadsads",
    sub_title:
      "oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinleroluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler test",
    creator: Asset.fromModule(require("../../assets/testorange.png")),
    creator_name: "Doğukan Karasansar",
    image: Asset.fromModule(require("../../assets/test2.png")),
    created_at: "11.04.2021",
  },
];

export const ListProject = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);
  return (
    <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
      {Platform.OS == "ios" ? (
        <Header
          statusBarProps={{ hidden: true }}
          backgroundColor={"#f50"}
          centerComponent={{
            text: "PROJECT MANAGEMENT",
            style: {
              color: "#fff",
              fontSize: 20,
            },
          }}
          rightComponent={{
            icon: "logout",
            color: "#fff",
            size: 25,
            onPress: () => signOut(),
          }}
        />
      ) : (
        <Header
          statusBarProps={{ hidden: false }}
          backgroundColor={"#f50"}
          centerComponent={{
            text: "PROJECT MANAGEMENT",
            style: { color: "#fff", fontSize: 20 },
          }}
        />
      )}
      <ScrollView style={{ zIndex: 500 }} showsVerticalScrollIndicator={false}>
        <FlatLists navigation={navigation} data={DATA} />
      </ScrollView>

      <AddButton navigation={navigation} />
    </View>
  );
};
