import { Asset } from "expo-asset";
import React from "react";
import { Dimensions, Image, Text, View } from "react-native";
import { AuthContext } from "../auth/AuthContext";
import { Error } from "../components/Error";
import { Loading } from "../components/Loading";
import { SendButton } from "../components/SendButton";
import { TextsInput } from "../components/TextsInput";

export const Login = ({ navigation }) => {
  const { signIn } = React.useContext(AuthContext);
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const width = Dimensions.get("window").width;
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#fff",
      }}
    >
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          bottom: 25,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.32,
          shadowRadius: 5.46,

          elevation: 9,
        }}
      >
        <Image
          source={Asset.fromModule(require("../../assets/authpageicon.png"))}
          width={'50%'}
          height={'50%'}
          style={{
            borderRadius: 15,
          }}
        />
      </View>
      {loading == true ? <Loading loading={loading} /> : null}
      {error == true ? <Error /> : null}
      <View>
        <TextsInput
          label={"Email"}
          autoCompleteType={"email"}
          icon={"envelope"}
          onChangeText={(val) => setEmail(val)}
          value={email}
        />
        <TextsInput
          label={"Şifre"}
          autoCompleteType={"password"}
          secureTextEntry={true}
          icon={"lock"}
          onChangeText={(val) => setPassword(val)}
          value={password}
        />
      </View>
      <View style={{ alignItems: "center" }}>
        <SendButton
          title={"Giriş"}
          buttonStyle={{ backgroundColor: "#f50" }}
          titleStyle={{ color: "#fff" }}
          onPress={() => {
            signIn({ email, password }, setLoading, setError);
          }}
        />
      </View>
      <View style={{ left: width / width + 10, top: 15 }}>
        <Text style={{ color: "grey" }}>
          Hesabın eğer yoksa lütfen{" "}
          <Text
            style={{ color: "#f50" }}
            onPress={() => navigation.navigate("Register")}
          >
            Kayıt ol!
          </Text>
        </Text>
      </View>
    </View>
  );
};
