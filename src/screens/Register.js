import { Asset } from "expo-asset";
import React from "react";
import { Dimensions, Image, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AuthContext } from "../auth/AuthContext";
import { Error } from "../components/Error";
import { Loading } from "../components/Loading";
import { SendButton } from "../components/SendButton";
import { TextsInput } from "../components/TextsInput";

export const Register = ({ navigation }) => {
  const { signUp } = React.useContext(AuthContext);
  const [email, setEmail] = React.useState("");
  const [name, setName] = React.useState("");
  const [password_confirmation, setConfirmation] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const width = Dimensions.get("window").width;
  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#fff",
      }}
    >
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          bottom: 25,
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 4,
          },
          shadowOpacity: 0.32,
          shadowRadius: 5.46,

          elevation: 9,
        }}
      >
        <Image
          source={Asset.fromModule(require("../../assets/authpageicon.png"))}
          width={1}
          height={1}
          style={{
            borderRadius: 15,
          }}
        />
      </View>
      {loading == true ? <Loading loading={loading} /> : null}
      {error == true ? <Error /> : null}
      <View>
        <TextsInput
          label={"Ad"}
          autoCompleteType={"name"}
          icon={"user-plus"}
          onChangeText={(val) => setName(val)}
          value={name}
        />
        <TextsInput
          label={"Email"}
          autoCompleteType={"email"}
          icon={"envelope"}
          onChangeText={(val) => setEmail(val)}
          value={email}
        />
        <TextsInput
          label={"Şifre"}
          autoCompleteType={"password"}
          secureTextEntry={true}
          icon={"lock"}
          onChangeText={(val) => setPassword(val)}
          value={password}
        />
        <TextsInput
          label={"Şifre tekrar"}
          autoCompleteType={"password"}
          secureTextEntry={true}
          icon={"lock"}
          onChangeText={(val) => setConfirmation(val)}
          value={password_confirmation}
        />
      </View>
      <View style={{ alignItems: "center" }}>
        <SendButton
          title={"Kayıt"}
          buttonStyle={{ backgroundColor: "#f50" }}
          titleStyle={{ color: "#fff" }}
          onPress={() => {
            signUp(
              { name, email, password, password_confirmation },
              setLoading,
              setError
            );
          }}
        />
      </View>
      <View style={{ left: width / width + 10, top: 15 }}>
        <Text style={{ color: "grey" }}>
          Hesabınız varsa lütfen{" "}
          <Text
            style={{ color: "#f50" }}
            onPress={() => navigation.navigate("Login")}
          >
            Giriş yap!
          </Text>
        </Text>
      </View>
    </View>
  );
};
