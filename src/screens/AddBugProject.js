import React from "react";
import { Text, View } from "react-native";
import { ExitButton } from "../components/ExitButton";

export const AddBugProject = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <ExitButton navigation={navigation} />
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Bug Ekleme Sayfası</Text>
      </View>
    </View>
  );
};
