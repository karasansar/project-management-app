import React from "react";
import { Text, View } from "react-native";
import { ExitButton } from "../components/ExitButton";

export const AddProject = ({ navigation }) => {
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <ExitButton navigation={navigation} />
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Proje Ekleme Sayfası</Text>
      </View>
    </View>
  );
};
